package org.lq;

import io.swagger.annotations.*;
import org.lq.annotaion.MyAnnotaionForSwagger;
import org.lq.entity.MyEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/swagger")
@Api(tags = {"MyController","Swagger学习控制器"},description = "这是测试api类型的信息")
/**
 * Api--描述当前类型生成的帮助文档的信息
 *  tags 给当前类型定义别名,可以有多个,定义别名,在ui视图中就显示几个控制访问的菜单
 *  description 给当前类型生成的帮助文档定义的一个描述信息
 */
public class MyController {
    @MyAnnotaionForSwagger
    @RequestMapping("/reg")
    public String req(){
        return "req";
    }
    @GetMapping("/get")
    public String get(
            @ApiParam(name = "用户名(a)",value = "新增用户时的用户名",required = true) String a,
            @ApiParam(name = "密码(b)",value = "新增用户时的密码",required = true)
            String b){
        return "/get";
    }
    @ApiIgnore  //@ApiIgnore 当前注解描述的方法或类型不生成api帮助文档
    @PostMapping("/post")
    @ApiOperation(value = "post方法执行数据新增操作",notes = "Swagger学习使用处理post请求的方法")
    public String post(String a,String b){
        return "post";
    }
    @GetMapping("/test")
    //@ApiImplicitParam(name = "m",value = "m参数描述",required = false,paramType = "字符串类型",dataType ="键值对")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "m",value = "m参数描述",required = false,paramType = "字符串类型",dataType ="键值对"),
            @ApiImplicitParam(name = "n",value = "m参数描述",required = true,paramType = "字符串(String)类型",dataType ="键值对")
    })
    public String test  (String m,String n){
        return "test";
    }
    //返回类型时MyEntity
    @RequestMapping("/myEntity")
    public MyEntity getMyEntity(){
        return new MyEntity();
    }

}
