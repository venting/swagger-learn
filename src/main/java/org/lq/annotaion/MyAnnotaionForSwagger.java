package org.lq.annotaion;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Target描述当前的注解可以定义在什么资源上
 * 定义具体的资源包括
 * ElementType.Method,可以定义在方法
 * ElemeentType.Type 可以定义在属性
 * ElementType.Field 可以定义在属性上
 * ElementType.Parameter 可以定义在方法参数上
 * @Retention 当前注解在什么时候有效
 * 属性value
 * 定义具体的生效标记
 * RetionPolicy.RUNTIME运行时有
 * RetentionPolicy.Source 源码中有效
 * RetentionPolicy.class 字节码有效
 */
@Target(value = {ElementType.METHOD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface MyAnnotaionForSwagger {
    //自定义注解中的属性.相当于@MyAnnotaionForSwagger(value="");
    String value() default "";
}
