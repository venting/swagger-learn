package org.lq.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.annotation.sql.DataSourceDefinition;
import java.io.Serializable;
@ApiModel(value = "这是自定义实体",description = "MyEntity存储用户数据") //ApiMode时描述一个实体类型,这个实体类型如果成为一个控制器方法,
    // 如果成为任何一个生成api帮助文档方法返回值类型,则注解被解析
public class MyEntity implements Serializable {
    @ApiModelProperty(value = "主键"
            ,name = "id"
            ,required = false
            ,example = "1"
            ,hidden = false)
    private int id;
    @ApiModelProperty(value = "姓名"
            ,name = "姓名"
            ,required = true
            ,example = "张三"
            ,hidden = false)
    private String name;
    @ApiModelProperty(value = "密码"
            ,name = "密码"
            ,required = true
            ,example = "123"
            ,hidden = false)
    private String password;

    public MyEntity(int id, String name, String password) {
        this.id = id;
        this.name = name;
        this.password = password;
    }

    public MyEntity() {
    }

    @Override
    public String toString() {
        return "MyEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
