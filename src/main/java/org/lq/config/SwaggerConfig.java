package org.lq.config;

import org.lq.annotaion.MyAnnotaionForSwagger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import  com.google.common.base.Predicates;
import  springfox.documentation.builders.RequestHandlerSelectors;

@Configuration
public class SwaggerConfig {
    //创建Docket对象并使用Spring容器管理
    /*
        Docket是Swagger全局配置对象
     */
    @Bean
    public Docket getDocket(){
        //API帮助文档的描述信息
        ApiInfo apiInfo = new ApiInfoBuilder().title("swagger框架学习帮助文档")//文档标题
                .description("Swagger框架帮助文档详细描述，Swagger框架是一个用户开发RestApi")
                //配置swagger文档主题内容
                .contact(new Contact("swagger开发文档-ming",//文档发布者的名称
                        "http://39.96.0.80:8081", //文档发布者的网站地址
                        "admin@qq.com")). //文档发布者的电子邮箱
                version("1.1").build();
        Docket d = new Docket(DocumentationType.SWAGGER_2);
        //给上下问dockt对象配置描述信息
        d.apiInfo(apiInfo);
       d= d.select() //获取Docket中的选择器,构建选择器的.扫描哪个包的注解
        .apis(
                Predicates.and(
                Predicates.not(//Predicates.not 取反 false - true true-false
                RequestHandlerSelectors.withMethodAnnotation(//当方法上有注解时返回true(即扫描所有的注解  )
                        MyAnnotaionForSwagger.class))
                ,RequestHandlerSelectors.basePackage("org.lq"))
                )
               .paths(
                       Predicates.or( //多个条件符合一个即可通过
                               PathSelectors.regex("/swagger/.*"),
                               PathSelectors.regex("/.*")
                       )

               )//约束生成API文档的路径地址
               .build();
    return d;
    }
}
