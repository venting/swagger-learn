package org.lq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2 //是springfox提供的注解代表swagger相关技术开启
public class SwaggerLearnApplication {

    public static void main(String[] args) {
        SpringApplication.run(SwaggerLearnApplication.class, args);
    }

}
